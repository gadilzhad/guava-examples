package com.arshakyan.GuavaExamples;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by ezalor on 13.05.14.
 */
public class GuavaStringsTests {


    @Test
    public void SplitterUsageFromStringToList(){
        String someStringSeparatedWithComa = "Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday";

        List<String> list = Splitter.on(",").trimResults().splitToList(someStringSeparatedWithComa);
        assertEquals(list.size(), 7);

        Set<String> actualSet = ImmutableSet.copyOf(list);
        ImmutableSet<String> expectedSet = ImmutableSet.of("Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
                                                            "Saturday", "Sunday");
        assertTrue(actualSet.containsAll(expectedSet));
    }

    @Test
    public void SplitterUsageFromStringContainsEmptyEntriesToList(){
        String someStringSeparatedWithComa = "Monday,, Tuesday, Wednesday,     , Thursday, Friday, Saturday, Sunday";

        List<String> list = Splitter.on(",").trimResults().omitEmptyStrings().splitToList(someStringSeparatedWithComa);
        assertEquals(list.size(), 7);

        Set<String> actualSet = ImmutableSet.copyOf(list);
        ImmutableSet<String> expectedSet = ImmutableSet.of("Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
                "Saturday", "Sunday");
        assertTrue(actualSet.containsAll(expectedSet));
    }


    @Test
    public void TestWorkOfJoiner(){
        Joiner joiner = Joiner.on(':').skipNulls();
        String joinStr = joiner.join("American", "Psycho", "One", "Love");
        String expectedStr = "American:Psycho:One:Love";


        assertEquals(joinStr, expectedStr);
    }

    @Test
    public void TestWordOfCharMatcher(){
        String rareString = "Aa123BurBurBur";
        String onlyDigits = "123";
        String withoutLowercaseAndDigits = "ABBB";

        assertEquals(onlyDigits, CharMatcher.DIGIT.retainFrom(rareString));
        assertEquals(withoutLowercaseAndDigits, CharMatcher.DIGIT.or(CharMatcher.JAVA_LOWER_CASE).removeFrom(rareString));
    }

}
