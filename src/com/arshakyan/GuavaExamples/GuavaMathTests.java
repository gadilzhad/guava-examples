package com.arshakyan.GuavaExamples;

import com.google.common.math.*;
import org.junit.Test;

import java.math.BigInteger;
import java.math.RoundingMode;

import static org.junit.Assert.*;

/**
 * Created by ezalor on 14.05.14.
 */
public class GuavaMathTests {

    @Test
    public  void TestWorkOfLog2Method(){
        assertEquals(BigIntegerMath.log2(new BigInteger("8"), RoundingMode.CEILING), 3);
    }

    @Test
    public  void TestWorkOfLog10Method(){
        assertEquals(BigIntegerMath.log10(new BigInteger("100000"), RoundingMode.CEILING), 5);
    }

    @Test
    public void TestWorkOfIsPowerOfTwoMethod(){
        assertTrue(BigIntegerMath.isPowerOfTwo(new BigInteger("2048")));
    }

    @Test
    public void TestWorkOfFactorialMethod(){
        assertEquals(BigIntegerMath.factorial(6), new BigInteger("720"));
    }

    @Test
    public void TestWorkOfGCDFunction(){
        assertEquals(LongMath.gcd(16, 64), 16);
    }


}
